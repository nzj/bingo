 @var trs = tag.children, check=(tag.check!"none");
 @var rowid = tag.rowid!=null?tag.rowid:"id"; //用于删除或者更新
 @var isScroll = tag.style.scroll;
 @var queryFormId = tag.queryForm;
 @var rsp = tag.data;
 @if(rsp.status.code!=0){
 @ include("_error.tag"){};return ;
 @}
 @var result = rsp.content.result;
 @if(result.~size==0){
 查询无结果
 @return ; 
 @} 
 <dd class="table-responsive" style="${isScroll?'overflow-y:hidden;'}">
            <table class="table table-striped" id="${tag.id}">
              <thead>
                <tr>
                 <th style="display:none">
                    rowid
                 </th>
                 @if(check=="mutiple"){                 	
                  <th width="13">
                    <input type="checkbox" id="${tag.id}-checkall"/>
                  </th>
                 @}else if(check=="single"){
                  <th width="13">
                    
                  </th>                 
                 @} for(tr in trs){
                    @var hidden =tr.style.hidden?"display:none";
                  <th style="${hidden}" >${tr.name}</th>
                 @}
                  
                </tr>
              </thead>
              <tbody>
              	@for(row in result){
              	@	  @tag.binds(row,rowLP.index);          	
                <tr>
                  <td style="display:none" value="${row[rowid]}">                   	
                  </td>
               	  @if(check=="mutiple"){
                  <td >
                    <input type="checkbox" id="${tag.id}-check-${rowLP.index}"/>
                  </td>
                  @}else if(check=="single"){
                    <td >
                    	<input type="radio" name="${tag.id}-check" />
                  	</td>                  
                   @}for(tr in trs){
                   @var style= tr.style;
                   @var hidden =style.hidden?"display:none";                
                   <td style="${hidden}">
                  	@if(style.width!=null){ 
                  		@var trBody = tr.body;
                  	<p title="${trBody}" style="width:${style.width}">${trBody}</p>
                  	@}else{
                  	${tr.body}
                  	@}
                  </td>
                   @}
                </tr>
                @}
             
              </tbody>
            </table>        
        </dd>
        
        <bg:_pagger id="${tag.id}_page" data="${rsp.content}"  />
        
		<script>		
		$("#${tag.id}").table({
			form:"${queryFormId}",
			recNumPerPage:${rsp.content.recNumPerPage}
					
		});
		
		</script>
@var size=parseDouble(tag.size!"1");
@var divWidth=100;
@var inputWidth=100;
@var colnum=1;
@if(tag.parent.tagName=="inputGroup" || tag.parent.tagName=="queryArea" ){
@column=tag.parent.colnum;
@}else{
@column=tag.parent.parent.colnum;
@}
@if(size >= 1){
@divWidth=1/parseInt(column)*100*size;
@}else{
@divWidth=1/parseInt(column)*100;
@inputWidth=size*100;
@}

@var tagStartName = tag.startName,tagEndName = tag.endName;
@var tagStartValue = eval(tagStartName),tagEndValue = eval(tagEndName);
@var startId = tag.startId!tagStartName,endId = tag.endId!tagEndName;



<dl class="dl-input" style="width:${divWidth}%">
    @include("_inputHead.tag"){}
    <dd>
        <div class="input-group in-date2 fl">
            <input id="${startId}" type="text" name="${tagStartName}" value="${tagStartValue}"
                    class="form-control" placeholder="${tag.hint}"  aria-describedby="send-addon">
            <span class="input-group-addon glyphicon glyphicon-calendar" "></span>
       
        </div>
        
        <span class="fl">&nbsp;--&nbsp;</span>
         <div  class="input-group in-date2 fl">
            <input id="${endId}" type="text" name="${tagEndName}" value="${tagEndValue}"
                    class="form-control" placeholder="${tag.hint}"  aria-describedby="send-addon">
            <span class="input-group-addon glyphicon glyphicon-calendar" ></span>
       
        </div>
        
        


        @if(tag.parent.tagName!="queryArea" ){
        @include("_inputFoot.tag"){}
        @}
    </dd>
</dl>

<script>
    $(document).ready(function(){
        $('#${startId}').datetimepicker({
            changeMonth: true,
            changeYear: true,
            showTimepicker: ${tag.style.time!false},
            dateFormat:'yy-mm-dd'
        });
        
        $('#${endId}').datetimepicker({
            changeMonth: true,
            changeYear: true,
            showTimepicker: ${tag.style.time!false},
            dateFormat:'yy-mm-dd'
        });
   });
</script>
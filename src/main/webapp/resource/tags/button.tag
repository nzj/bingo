@var style = tag.style;
@var colorStyle = "btn-add"; //通常的样式
@if(style.primary){
@colorStyle="btn-add";
@}else if(style.secondary){
@colorStyle="btn-del";
@}else if(style.link){
@colorStyle="blue1";
@}


@ if(style.disable){
@  if(style.link){
<span style ="cursor: not-allowed"   id="${tag.id}" >${tag.body}</span>
@  }else{
<span style ="cursor: not-allowed"   id="${tag.id}"  class="${ colorStyle }" >${tag.body}</span>
@  }
@}else{
<a href="javascript:void(0)" class="${ colorStyle }"  id="${tag.id}"  ${tag.onclick!=null?('onclick='+tag.onclick) } >${tag.body}</a>
@}
 
 
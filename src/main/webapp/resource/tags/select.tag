@var headerKey=tag.headerKey!"";
@var headerValue=tag.headerValue!"";
@var emptyOption=(tag.emptyOption!"false")=="true";

@var size=parseDouble(tag.size!"1");
@var divWidth=100;
@var inputWidth=100;
@var colnum=1;
@if(tag.parent.tagName=="inputGroup" || tag.parent.tagName=="queryArea" ){
@column=tag.parent.colnum;
@}else{
@column=tag.parent.parent.colnum;
@}
@if(size >= 1){
@divWidth=1/parseInt(column)*100*size;
@}else{
@divWidth=1/parseInt(column)*100;
@inputWidth=size*100;
@}

@var tagName=tag.name;
@var map = eval(tag.list);
@var value = tag.value!eval(tagName);

<dl class="dl-input" style="width:${divWidth}%">
    @include("_inputHead.tag"){}
    <dd>
        <select class="form-control in-select1">
            @if(headerKey !="" && headerValue !=""){
                <option value="${headerKey}"
                        >${headerValue}</option>
            @}
            @if(emptyOption){
                <option value=""></option>
            @}
            @for(item in map){
                <option value="${item.key}" ${item.key==value?'selected = "selected"'} >${item.value}</option>
            @}

        </select>

        @if(tag.parent.tagName!="queryArea" ){
        @include("_inputFoot.tag"){}
        @}
    </dd>

</dl>
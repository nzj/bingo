@ var validate = tag.validate!"";
@ var required= false;
@ if(strutil.index(validate,"require")!=-1 ){
@ 	required = true;
@}else{ 
@ 	var validates = tag.children;
@ 	var requireTag = matchTag(validates,"name","require");
@ 	required= requireTag.~size!=0;
@}
<dt>
    @if(required) {
    <span class="red">*</span>
    @}
    @if(isEmpty(tag.label)) {
    &nbsp
    @} else {
    ${tag.label}
    @}
</dt>
@var size=parseDouble(tag.size!"1");
@var divWidth=100;
@var inputWidth=100;
@var colnum=1;
@if(tag.parent.tagName=="inputGroup" || tag.parent.tagName=="queryArea" ){
@column=tag.parent.colnum;
@}else{
@column=tag.parent.parent.colnum;
@}
@if(size >= 1){
@divWidth=1/parseInt(column)*100*size;
@}else{
@divWidth=1/parseInt(column)*100;
@inputWidth=size*100;
@}

@var tagName = tag.name;
@var tagDateValue = tag.value!eval(tagName);
@var tagValue = tagDateValue!=null?strutil.formatDate(tagDateValue,'yyyy-MM-dd'):null;

<dl class="dl-input" style="width:${divWidth}%">
    @include("_inputHead.tag"){}
    <dd>
        <div style="width:${inputWidth}%" class="input-group in-date fl">
            <input id="${tag.id}" type="text" name="${tagName}" value="${tagValue}"
                    class="form-control" placeholder="${tag.hint}"  aria-describedby="send-addon">
            <span class="input-group-addon glyphicon glyphicon-calendar" id="btn_${tag.id}"></span>
        </div>


        @if(tag.parent.tagName!="queryArea" ){
        @include("_inputFoot.tag"){}
        @}
    </dd>
     @if(tag.validate!=null||children.~size!=0)
     @include("_validate.tag"){};
</dl>

<script>
    $(document).ready(function(){
        $('#${tag.id}').datetimepicker({
            changeMonth: true,
            changeYear: true,
            showTimepicker: ${tag.style.time!false},
            dateFormat:'yy-mm-dd'
        });
   });
</script>
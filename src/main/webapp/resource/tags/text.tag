@ var children = tag.children;
@var size=parseDouble(tag.size!"1");
@var divWidth=100;
@var inputWidth=100;
@var colnum=1;
@if(tag.parent.tagName=="inputGroup" || tag.parent.tagName=="queryForm" ){
@column=tag.parent.colnum;
@}else{
@column=tag.parent.parent.colnum;
@}
@if(size >= 1){
@divWidth=1/parseInt(column)*100*size;
@}else{
@divWidth=1/parseInt(column)*100;
@inputWidth=size*100;
@}
@var tid=tag.id!tag.name;
@var tagName = tag.name;
@var tagValue = tag.value!eval(tagName);

<dl class="dl-input" style="width:${divWidth}%">
    @include("_inputHead.tag"){}
    <dd>
        <input id="${tid}" type="text" name="${tagName}" value="${tagValue}"
               class="form-control" placeholder="${tag.hint}" style="width:${inputWidth}%"/>
        @if(tag.parent.tagName!="queryArea" ){
        @include("_inputFoot.tag"){}
        @}
    </dd>
    @if(tag.validate!=null||children.~size!=0)
    @include("_validate.tag"){};
</dl>
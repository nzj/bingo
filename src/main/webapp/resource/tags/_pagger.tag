@var data = tag.data; //pageView //假设从1行，1页开始
@var recNum = data.recNum,list = data.result,currPage = data.currPage,pageNum=data.pageNum,recNumPerPage=data.recNumPerPage;
@var startRec = (currPage-1)*recNumPerPage+1,endRec = startRec+list.~size-1; //起始行
@var hasPrevious = currPage!=1,hasNext = currPage!=pageNum; //当前页是否是起始页，结束页
@ //pageview.recNumPerPage:10 pageview.currPage:2 翻页的时候还需要提交这来个参数
@ var id = tag.id!tag.rid;
<dd id="${id}">
      	<!--  翻页 -->
          <div class="nobor">每页显示 <span class="blue1">${startRec}-${endRec}</span> 条，共 <span class="blue1">${ recNum} </span> </div>
          	
          <div class="rpage-box">
              <span class="r-mar">共<strong class="blue1">${pageNum} </strong>页</span>
              <span>去第&nbsp;</span>
              <input type="text" name="pages" class="form-control intxt5" value="" />
              <span>&nbsp;页</span>
          </div>
          <ul class="pagination rpage-ul">
            <li ${currPage==1?'class="disabled"'}><a href="javascript:void(0)" page="1"><span aria-hidden="true">首页</span></a></li>
            <li ${!hasPrevious?'class="disabled"'}><a href="javascript:void(0)" page="${currPage>1?currPage-1:'#'}"  aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
            @ for(var i=1;i<=pageNum;i++){
            	@if(i==currPage){
            <li class="active"><a href="javascript:void(0)" page="${i}">${i}  <span class="sr-only">(current)</span></a></li>
            	@}else{
            <li><a href="javascript:void(0)" page="${i}">${i}</a></li>
            	@}
            @}
              <li ${!hasNext?'class="disabled"'} ><a href="javascript:void(0)" page="${currPage+1}" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
            <li><a href="javascript:void(0)" page="${pageNum}" ><span aria-hidden="true">末页</span></a></li>
          </ul>
              
        </dd>
      
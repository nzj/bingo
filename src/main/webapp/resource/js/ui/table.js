(function ( $ ) { 
	$.widget( "bingo.table", {	
		options: {	       
	        form:'queryForm',
	        recNumPerPage:10	
	    },
	    _create: function() {
	    	var that = this ;
	    	var  id = this.element.attr("id");
	    	//对多选进行设置
	    	var checkAll = $("#"+id+"-checkall").get(0);
	    	if(checkAll!=null){
	    		$(checkAll).click(function(){
	    			var checked = $(this).attr("checked");
	    			if(checked){
	    				that._checkAll(true);
	    			}else{
	    				that._checkAll(false);
	    			}
	    			
	    		});
	    	}
	    	//翻页标签
	    	var pagger = $("#"+id+"_page");
	    	//遍历所有<a>,
	    	var hrefs = $(pagger).find("a");	 
	    	for(var i=0;i<hrefs.length;i++){
	    		//取消链接	    		
	    		var li = $(hrefs[i]).parent();	    	
	    		$(li).on("click",function(){
	    			var p = $(this).find("a").attr("page");	    		
	    			that.query(p);
	    		})
	    	}
	    	
	    	// 查询表单按钮绑定	    	
	    	var btn = $("#"+this.options.form+"_btn_search");	
	    	
	    	//table 会在一个页面多次加载，但按钮只能加载一次
	    	$(btn).unbind("click");
	    	$(btn).on("click",function(){  
	    		that.formData = $("#"+that.options.form).serialize();
    			that.query(1);
    		})
    		
    		that.formData = $("#"+that.options.form).serialize();
	    	
	    	
	    	
	    },
	    // 行数
	    size:function(){
	    	var table = this.element;
	    	var trCount = $(table).find("tbody").children().length;
	    	return trCount;
	    }
	    ,
	    //返回被选中的一行的rowid
	    singleSelect:function(){
	    	var all = this.select();
	    	if(all.length>=1){
	    		return all[0];
	    	}else{
	    		return null;
	    	}
	    }
	    ,
	    //返回被选中的多行rowid
	    select:function(){
	    	var select = [];
	    	var table = this.element;
	    	var trs = $(table).find("tbody").children()
	    	for(var i=0;i<trs.length;i++){
	    		var tr = trs[i];
	    		var checkBoxTd = $(tr).find("td").get(1);
	    		var check = $(checkBoxTd).find("input").attr("checked") ;
	    		if(check){
	    			var rowTd =  $(tr).find("td").get(0);
	    			var value = $(rowTd).attr("value");
	    			select.push(value);
	    		}
	    		
	    	}
	    	return select;
	    }
	    
	    ,
	    query:function(page){
	    	var self = this ;	
    		var formData = this.formData;
    		if(formData==null){
    			this.formData = $("#"+this.options.form).serialize();	
    			formData = this.formData;
    		}
	    	
	    	if(page==null) page =1 ;
	    	
	    	formData = formData+"&"+"pageview.recNumPerPage="+this.options.recNumPerPage
	    	formData = formData+"&"+"pageview.currPage="+page
	    	
	    	var url = $("#"+this.options.form).attr("action");	
	    	$.ajax({
               
                type: "POST", //总是Post
                url:url,
                data:formData,
                async: true,
                error: function(request) {                
                  window.alert("服务器错误，请稍后再试");
                },
                success: function(data) {                	
                	var tableId = self.element.attr("id");
    				var tableContainer = $("#"+tableId).parent().parent();
    				var prev = tableContainer.prev();
    				//删除表格
    				tableContainer.remove();
    				//添加表格
    				prev.after(data)
                }
            });	 
	    	
	    }	    
	    ,
	    _checkAll:function(check){
	    	var table = this.element;
	    	var trs = $(table).find("tbody").children()
	    	for(var i=0;i<trs.length;i++){
	    		var tr = trs[i];
	    		var checkBoxTd = $(tr).find("td").get(1);	    		
	    		if(check){
	    			$(checkBoxTd).find("input").attr("checked",'true') 
	    		}else{
	    			$(checkBoxTd).find("input").removeAttr("checked",""); 
	    		}
	    		
	    	}
	    }
	    
	    
	}); 
		
}( jQuery ));

(function ( $ ) { 
	$.widget( "bingo.form", {
		options: {
	        success:function(rsp){
	        	if(rsp.result=="success"){
	        		alert(rsp.info);
	        	}else{
	        		alert(rsp.info);
	        	}
	        	
	        },
	        failure:function(){
	        	alert("服务器错误，请联系管理员");
	        	
	        }
	
	    },
	    _create: function() {
	    	var that = this;
	    	var id = this.element.attr("id");
	    	$("#"+id+"_submit").on("click",function(){
	    		that.ajaxSubmit(that.options.success,null);
	    	})
	    }
	    ,
	
	    ajaxSubmit:function(success,failure){	    	
	    	// 校验
	    	if(this.ajaxValidate()){
	    		var formData = $(this.element).serialize();
		    	this._submit(formData,success,failure);
	    	}
	    	
	    
	    
	    },
	    
	    _submit:function(formData,success,failure){
	    	success = success==null?this.options.success: success;
	    	failure = failure==null?this.options.failure: failure;
	    	var url = this.element.attr("action");	
	    	$.ajax({
               
                type: "POST", //总是Post
                url:url,
                data:formData,
                async: true,
                error: function(request) {                 
                   if(failure!=null){
                	   failure();
                   }                  
                },
                success: function(data) {                	
                     if(success!=null){
                    	 success(data);
                     }
                }
            });	    	
	    	
	    },	    
	    ajaxValidate:function() {
	        var rn = true;
	        var validate = $(this.element).data('validate');
	        if (validate) {
	            $(validate).each(function(){
	                if(BINGO.Validator.validate(this, false)){
	                    rn = false;
	                }
	            });

	        }

	        return rn;
	    }
	 
	    
	    
	}); 
		
}( jQuery ));
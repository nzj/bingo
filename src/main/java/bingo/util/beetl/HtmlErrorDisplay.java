package bingo.util.beetl;

import java.io.IOException;
import java.io.Writer;

import org.beetl.core.ConsoleErrorHandler;

public class HtmlErrorDisplay extends ConsoleErrorHandler {
	protected void println(Writer w, String msg)
	{
		
		try {
			w.write(msg);
			w.write("<br/>");
			w.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void print(Writer w, String msg)
	{
		
		try {
			w.write(msg);	
			w.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void printThrowable(Writer w, Throwable t)
	{
//		t.printStackTrace();
		
		
	}
}

package bingo.util.beetl;

import javax.servlet.http.HttpServletRequest;

import org.beetl.core.Context;
import org.beetl.core.Function;

public class TagNestFunction implements Function {

	
	public Object call(Object[] arg0, Context ctx) {
		HttpServletRequest request = (HttpServletRequest)ctx.getGlobal("request");
		TagChildrenContext tnc = (TagChildrenContext)request.getAttribute("tagContext");
		if(tnc==null){
			throw new RuntimeException("不在标签环境");
			
		}
		return tnc;
	}

}

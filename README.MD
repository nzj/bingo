##BingoUI是什么?
一个跨终端的前端框架。适用于pc，pad，和手机，开发者只需要一次开发，可以运行在多种终端上 



##BingoUI有哪些功能？

* 适应pc，pad，手机   
* 提供丰富的tag组件，如table，form，dialog，buttongroup
* 为每种页面组件提供了jquery插件
* 紧密结合后端程序，无缝嵌入在web应用中。


##与其他前端框架区别


* 其他前端框架不能完全适应多种终端，即使bootstrap，semantic这样的框架也是这样，他们表现风格仍然是pc式的，在mobile里缺少移动端风格
* 紧密结合后台，能非常容易的开发出完整web应用
* 视图降级，允许为不同终端制作特定页面以满足特定需求
* 相对于其他前端框架简化设计，BingoUI针对复杂企业应用设计，假设业务模型足够复杂，数据足够多，内容足够长
* 

##如何使用

下载工程，运行maven jetty:run 然后方位localhost:7777/table.html

##例子

### table

`代码部分`

	 <dl class="table-con" id="table">
       <bg:tableBar >
       		<bg:button id="add" style="disable" onclick="addNewCars()">新增车辆</bg:button>
       		<bg:button id="del"  onclick="addNewCars()">激活定位</bg:button>    
       		<bg:button id="send" >发送短信</bg:button> 
       		<bg:button id="add"  style="secondary" >发送短信</bg:button>       		
       </bg:tableBar>
        @ #ajax carsTable:{
        <div>       
			<bg:table id="cars-id" data="${pageView.result}" rowid="id" style="scroll" check="mutiple" var="row,index">
						
				<bg:tr name="车牌号"  style="width:200px;primary">${row.no}</bg:tr>
				<bg:tr name="司机"  style="hidden" >${row.name}</bg:tr>
				<bg:tr name="手机号" >${row.mobile}</bg:tr>
				<bg:tr name="地址" >${row.address}</bg:tr>
				
				<bg:tr name="操作123">
					<bg:button style="link;disable">编辑</bg:button> 
					<bg:button style="link"  onclick="testClick(${row.id},'${row.name}')" >删除</bg:button> 
					<bg:button style="link" >定位</bg:button> 
					<bg:button style="link" >开始</bg:button> 
				</bg:tr>
			 </bg:table> 
			 <bg:pagger data="${pageView}" table="cars-id"   form="queryform" />         
         </div>
        @}          
    </dl>
	 
`pc端效果:`
![tableonpc](sample-table.png)

`移动端效果`
略

##开发指南

### tag开发

略

### js开发

略

### 模拟后台数据

* 请求地址的数据对应于value 目录，如果请求路径是table.html ,对应的后台数据是table.html.var
    
    ```
     var pageView = {currPage:0,pageNum:7,recNum:234,recNumPerPage:10};
     var users = {};
    ```

* post请求总是对应于请求路径+post+var,比如table.html.post.var.如果post的url请求有querystring，
则queryString也是文件的一部分，如请求table.html?type=create ,那么对应的模拟文件是table.html_type=create.post.var

* 如果期望返回的是json，则table.html.var 应该包含一个叫json的变量

     ```
    var json = json({id:1,name:'hello'});
     ```
* 如果期望仅仅渲染模板的ajax模块，则table.html.var 还应包含一个ajax变量，值为ajax模块名称
    
    table.html
    ```
      @ #ajax carsTable:{
        <div>
        </div>
      @}  

    ```

    table.post.html.var
    
    ```
    var ajax = "carsTable";
    ```

## [标签使用说明](#tag001)  

公共属性

* id: 标签id，如果没有id，默认采用name
* name: 标签名称,如果有值，则按照名称赋值
* size： 标签相对宽度
* value: 标签值，如果没有则，按照name赋值。
* validate: 校验规则，如require。


### text

文本输入标签

	<bg:text label="aefe" id="test1" validate="require" name="car.address"  >
	
* 值是name对应的值，如果有value属性，优先采用value属性
* 可以嵌入vadidate 标签校验text,也可以简单的使用validate属性

### vadidate  

嵌入在可视化标签里，用来在提交表单的时候校验

	<bg:text label="aefe" id="test1" validate="require" name="car.address"  >
		<bg:validate name="require" info = "不能为空"/>
		<bg:validate  name="lenLimit" info="" min="1" max="10"/>
	</bg:text>	
* 请参考validate.js

### date  

日期输入控件

	<bg:date label="aefe" id="date"  validate="require" hint="日期" size="1" ></bg:date>

### dateRange

日期范围输入控件

	<bg:dateRange label="aefe" startId="startDate"   endId="endDate"  startName="startName" endName="endName" size="1"></bg:dateRange>
         
### select

下拉列表

	<bg:select label="aefe"  name="attr.valueType" list="cityMap" headerKey="0" headerValue="全部"></bg:select>

* headerKey 额外提供一个全部选项
* list 一个map结构，提供下拉选项

##关于作者

xiandafu,strider,nancy,wuweihua,peter.cui